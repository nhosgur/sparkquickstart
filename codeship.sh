#!/bin/bash
curl -sSL https://raw.githubusercontent.com/codeship/scripts/master/packages/mongodb.sh | bash -s
mkdir -p node_modules
cd web
cp -R ../node_modules .
#npm config set cache "${HOME}/cache/npm/"
#npm install -g npm@latest
npm install
npm install -g bower
bower install
cd ../server
ant create-databases
ant install-ivy
ant resolve
ant dist
ant auth-db-schema
ant shared-db-schema
