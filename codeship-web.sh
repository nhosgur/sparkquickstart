#!/bin/bash
#mkdir -p node_modules
cd web
cp -R ../node_modules .
npm config set cache "${HOME}/cache/npm/"
npm install -g npm@latest
npm install -g bower
bower install
yarn install
npm install -g jest@^23.6
