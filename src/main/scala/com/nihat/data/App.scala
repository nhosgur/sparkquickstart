//package com.nihat.data
//import org.apache.spark.sql.{Encoders, SparkSession}
//
//
//
//object App extends App{
//
//  val spark = SparkSession.builder.master("local[*]").appName("Simple Application").getOrCreate()
//
//  val logFile = "us.csv" // Should be some file on your system
//
//  case class Person(name: String, age: Long)
//
//  val casultiesTable = "casulties"
//  //val df = spark.read.json("citylots.json")
//  val df = spark.read.format("csv")
//    .option("sep", ",")
//    .option("inferSchema", "true")
//    .option("header", "true")
//    .load("casulties.csv")
//
//  implicit val personEncoder = Encoders.product[Person]
//
//  df.as[Person]
//
//
//  df.printSchema()
//  df.createOrReplaceTempView(casultiesTable)
//  val lotsDF = spark.sql(s"" +
//    s" SELECT country,sum(deaths) total FROM $casultiesTable" +
//    s" group by country order by total desc")
//  lotsDF.collect().foreach(println)
//
//  import spark.implicits._
//  // This import is needed to use the $-notation
//  // Print the schema in a tree format
//
//
//  val logData = spark.read.textFile(logFile).cache()
//
//
//
//  val numAs = logData.filter(line => line.contains("a")).count()
//  val numBs = logData.filter(line => line.contains("b")).count()
//
//  println(s"Lines with a: $numAs, Lines with b: $numBs")
//
//
//
//
//
//
//
//  // Encoders are created for case classes
//  val caseClassDS = Seq(Person("Andy", 32)).toDS()
//  val personDataSet = caseClassDS.as[Person]
//
//  caseClassDS.show()
//  // +----+---+
//  // |name|age|
//  // +----+---+
//  // |Andy| 32|
//  // +----+---+
//
//  // Encoders for most common types are automatically provided by importing spark.implicits._
//  val primitiveDS = Seq(1, 2, 3).toDS()
//  primitiveDS.map(_ + 1).collect() // Returns: Array(2, 3, 4)
//
//  // DataFrames can be converted to a Dataset by providing a class. Mapping will be done by name
//  val path = "examples/src/main/resources/people.json"
//  val peopleDS = spark.read.json(path).as[Person]
//  peopleDS.show()
//
//
//
//  // For implicit conversions from RDDs to DataFrames
//  import spark.implicits._
//
//  // Create an RDD of Person objects from a text file, convert it to a Dataframe
//  val peopleDF = spark.sparkContext
//    .textFile("examples/src/main/resources/people.txt")
//    .map(_.split(","))
//    .map(attributes => Person(attributes(0), attributes(1).trim.toInt))
//    .toDF()
//  // Register the DataFrame as a temporary view
//  peopleDF.createOrReplaceTempView("people")
//
//  // SQL statements can be run by using the sql methods provided by Spark
//  val teenagersDF = spark.sql("SELECT name, age FROM people WHERE age BETWEEN 13 AND 19")
//
//  // The columns of a row in the result can be accessed by field index
//  teenagersDF.map(teenager => "Name: " + teenager(0)).show()
//  // +------------+
//  // |       value|
//  // +------------+
//  // |Name: Justin|
//  // +------------+
//
//  // or by field name
//  teenagersDF.map(teenager => "Name: " + teenager.getAs[String]("name")).show()
//  // +------------+
//  // |       value|
//  // +------------+
//  // |Name: Justin|
//  // +------------+
//
//  // No pre-defined encoders for Dataset[Map[K,V]], define explicitly
//  implicit val mapEncoder = org.apache.spark.sql.Encoders.kryo[Map[String, Any]]
//  // Primitive types and case classes can be also defined as
//  // implicit val stringIntMapEncoder: Encoder[Map[String, Any]] = ExpressionEncoder()
//
//  // row.getValuesMap[T] retrieves multiple columns at once into a Map[String, T]
//  teenagersDF.map(teenager => teenager.getValuesMap[Any](List("name", "age"))).collect()
//  // Array(Map("name" -> "Justin", "age" -> 19))
//
//
//  spark.stop()
//
//}