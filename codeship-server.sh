#!/bin/bash
curl -sSL https://raw.githubusercontent.com/codeship/scripts/master/packages/mongodb.sh | bash -s
cd server
ant create-databases
ant install-ivy
ant resolve
ant dist
ant auth-db-schema
ant shared-db-schema
